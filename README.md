# Open git.sr.ht repos in Gitpod

**Where my files?!?** Cloned repos from `git.sr.ht` are in the `data/repos` directory.

## 👇 Start here for users

1. Sign in to Gitpod using your existing GitLab.com account. If you do not have an GitLab.com account, you can sign up using your existing social account. This is required for Git authentication reasons when you decided to customize the repo for your project's needs.
2. Prefix any `git.sr.ht` URL with `gitpod.io/#SRHT_REPO_URL=` and suffix it with `#/https://gitlab.com/ajhalili2006-experiments/hutpod` (see <https://github.com/gitpod-io/gitpod/issues/11796> for the feature request in meanwhile, this will opens GitLab.com mirror of the repo) and press Enter.
3. You'll be prompted for your sr.ht personal access token to add Gitpod-specific SSH key to your account. Reply with `y` and follow prompts. Note that the SSH key generated in this step is passwordless, please be caution when keeping this key outside Gitpod. If you prefer to use your own key, just encode that key to base64 and add the resulting text as `SRHT_GIT_SSH_KEY` variable in <https://gitpod.io/variables>.
4. The repository you want to open will be cloned after SSH key checks. For private repositories, we'll retry automatically for you. Your repositories are stored in `$THEIA_WORKSPACE_ROOT/data/repos` directory.
5. Happy coding! If you need customizations, [ask your maintainer to fork the repo](https://man.sr.ht/~ajhalili2006-experiments/hutpod/for-maintainers/setup.md).

## License and contributing to project

AGPL v3 and above only.

By contributing to the project, you agree to [Recap TIme Squad's Community Code of Conduct](https://recaptime.page.link/codeofconduct), alongside [sr.ht's terms of service](https://man.sr.ht/terms.md).
