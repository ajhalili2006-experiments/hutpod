# Contributing to the project

For information on how to contribute, please [see the project wiki over at man.sr.ht][wiki]. The rest of this document are mostly legalese.

## Code of conduct

While this project is not under Recap Time Squad's namespace on both GitLab.com and sr.ht, its [Community Code of Conduct][coc] still applies here.

[coc]: https://recaptime.page.link/codeofconduct
[wiki]: https://man.sr.ht/~ajhalili2006-experiments/hutpod

## DCO and CLA

Signing [Recap Time Squad CLA](https://wiki.recaptime.eu.org/wiki/CLA) doesn't apply here, nor it's accepted on Andrei Jiroh's projects (including experimental projects). By submitting patches to the repo, you agree to the terms under [the Linux DCO](https://developercertificate.org/).
