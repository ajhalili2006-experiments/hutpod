#!/usr/bin/env bash
if [ -n "$DEBUG" ] || [ -n "$GITPOD_HEADLESS" ]; then
    set -x
fi

# misc housekeeeping, goes here, although left blank in meanwhile