#!/usr/bin/env bash

if ! command -v hut >> /dev/null; then
  echo "error: hut CLI isn't installed in this workspace or container, check your image's"
  echo "error: Dockerfile or consult the following URL for the syntax:"
  echo "error:   https://gitlab.com/ajhalili2006-experiments/hutpod/blob/main/.gitpod/docker/Dockerfile#L13-L20"
  echo "error: If that's not the case, try re-creating/rebuilding the workspace and try again."
  exit 1
fi

# Check whenever we're on Gitpod or Codespaces and adjust the config path
# as needed.
GP_HUT_CONFIG="/workspace/.hut-cli"
CS_HUT_CONFIG="/workspaces/.hut-cli"
if [[ $GITPOD_INSTANCE_ID != "" ]]; then
  [ ! -d $GP_HUT_CONFIG ] && mkdir $GP_HUT_CONFIG
  exec hut --config "$GP_HUT_CONFIG" "$*"
elif [[ $CODESPACE_NAME != "" && $CODESPACE == "true" ]]; then
  [ ! -d $CS_HUT_CONFIG ] && mkdir $CS_HUT_CONFIG
  exec hut --config "$CS_HUT_CONFIG" "$*"
else
  exec hut "$*"
fi